// 封装全局组件库

// 引入组件
import PageTools from '@/components/PageTools'
import UploadExcel from '@/components/UploadExcel'
import ImageUpload from '@/components/ImageUpload'
import ScreenFull from '@/components/ScreenFull'
import ThemePicker from '@/components/ThemePicker'
import Lang from '@/components/Lang'
import TagsView from '@/components/TagsView'

// 暴露
export default {
  // 准备一个属性名为 install 的方法,所有逻辑都在这里面写
  // 这个方法可以接受一个形参,就是 vue 包
  install(Vue) {
    // 将需要注册的全局组件写入
    Vue.component('PageTools', PageTools)
    Vue.component('UploadExcel', UploadExcel)
    Vue.component('ImageUpload', ImageUpload)
    Vue.component('ScreenFull', ScreenFull)
    Vue.component('ThemePicker', ThemePicker)
    Vue.component('Lang', Lang)
    Vue.component('TagsView', TagsView)
  }
}
