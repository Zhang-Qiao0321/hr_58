import Cookies from 'js-cookie'

const TokenKey = 'vue_admin_template_token'
const TimeKey = 'hrsaas-timestamp-key'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

// 声明用于记录个获取时间戳 cookise 的函数
export function setTimeStamp() {
  return Cookies.set(TimeKey, Date.now())
}

export function getTimeStamp() {
  return Cookies.get(TimeKey)
}
