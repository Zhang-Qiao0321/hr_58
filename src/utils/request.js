import router from '@/router'
import store from '@/store'
import axios from 'axios'
import { Promise } from 'core-js'
// 引入 Element 消息提示
import { Message } from 'element-ui'
import { getTimeStamp } from './auth'

// 登录后超时时间 1小时 单位是毫秒
const timeout = 60 * 60 * 1000 * 2

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})

// request interceptor
// 请求拦截器有两个参数: 一个处理成功,一个处理失败
// 虽然我们现在没什么失败的可能,但是还是可以写一个回到做保险
service.interceptors.request.use(
  async config => {
    // 统一实现 token 注入
    // 判断有登录过,是否有 token 值 同时 没有设置请求头的 token 验证
    if (store.getters.token && !config.headers.Authorization) {
      // 以上判断是否有 token
      // 现在要多加一个处理逻辑,判断是否登录过时
      // 封装一个函数检查是否登录过期,如果返回 true 则是过期,否则一切正常
      if (checkTimeout()) {
        // 过期处理
        // 1.执行退出登录处理
        await store.dispatch('user/logout')
        // 2. 跳转到登录页
        router.push('/login')
        // 3. 提示
        Message.error('token 过期')
        return Promise.reject('token 过期').catch(() => {})
      } else {
        // 注入 token
        config.headers.Authorization = 'Bearer ' + store.getters.token
      }
    }
    return config
  }, err => {
    // 提示
    Message.error('请求失败')
    // 返回失败信息
    return Promise.reject(new Error(err))
  }
)

// response interceptor
// 响应拦截器处理错误, 在响应拦截器当中,可能有两种错误类型
// res: 第一个用来处理 200 状态码的成功请求(数据不一定对)
// err: 第二个函数则是处理失败的请求和 404 500 的错误码
service.interceptors.response.use(
  res => {
    // 结构出请求得到的数据
    const { success, data, message } = res.data
    // 根据 success 成功下决定
    if (success) {
      // 提示信息
      // Message.success('登录成功')
      // success 为真, 正常返回数据
      return data
    } else {
      // 提示错误信息
      Message.error(message || '数据错误')
      // 业务已经错误了 还能进then ? 不能 ！ 应该进catch
      // 返回错误信息
      // reject: 为了使用的时候可以继续链式调用
      return Promise.reject(new Error(message))
    }
  }, async err => {
    // console.dir(err)
    // error 信息 里面 response的对象
    if (err.response && err.response.data && err.response.data.code === 10002) {
      // 1.执行退出登录处理
      await store.dispatch('user/logout')
      // 2. 跳转到登录页
      router.push('/login')
      // 3. 提示
      Message.error('登录过期')
      return Promise.reject('登录过期').catch(() => { console.log('登录过期') })
    } else {
      Message.error(err.message) // 提示错误信息
      return Promise.reject(err) // 返回执行的错误信息
    }
  }
)

export default service

// 封装登录超时
const checkTimeout = () => {
  // 登录的时间
  const loginTimeStamp = getTimeStamp()
  // 当前时间
  const nowTimeStamp = Date.now()
  // 比较时间
  return nowTimeStamp - loginTimeStamp > timeout
}
