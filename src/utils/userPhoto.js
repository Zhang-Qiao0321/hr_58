import Vue from 'vue'
// 在Vue原型上挂载默认头像
import userPhoto from '@/assets/common/bigUserHeader.png'

Vue.prototype.$userPhoto = userPhoto

export default userPhoto
