import request from '@/utils/request'

// 获取所有角色列表
// Path： /sys/role
// Method： GET
export function getRoleList(params) {
  return request({
    method: 'get',
    url: '/sys/role',
    params
  })
}

// 根据id查询企业
// Path： /company/{id}
// Method： GET
export function getCompanyInfo(companyId) {
  return request({
    method: 'get',
    url: `/company/${companyId}`
  })
}

// 根据ID获取角色详情
// Path： /sys/role/{id}
// Method： GET
export function getRoleDetail(id) {
  return request({
    method: 'get',
    url: `/sys/role/${id}`
  })
}

// 根据ID更新角色 编辑
// Path： /sys/role/{id}
// Method： PUT
export function updateRole(data) {
  return request({
    method: 'put',
    url: `/sys/role/${data.id}`,
    data
  })
}

// 根据ID删除角色
// Path： /sys/role/{id}
// Method： DELETE
export function deleteRole(id) {
  return request({
    method: 'delete',
    url: `/sys/role/${id}`
  })
}

// 添加角色
// Path： /sys/role
// Method： POST
export function addRole(data) {
  return request({
    method: 'post',
    url: `/sys/role`,
    data
  })
}

// 给角色分配权限
// Path： /sys/role/assignPrem
// Method： PUT
export function assignPerm(data) {
  return request({
    url: '/sys/role/assignPrem',
    method: 'put',
    data
  })
}
