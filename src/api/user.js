import request from '@/utils/request'

// 登录
export function login(data) {
  return request({
    method: 'post',
    url: '/sys/login',
    data
  })
}

// 获取用户基本资料
export function getInfo(token) {
  return request({
    method: 'post',
    url: '/sys/profile'
    // 请求拦截器中 注入了 token
    // headers: {
    //   Authorization: 'Bearer ' + store.getters.token
    // }
  })
}

// 获取用户头像
export function getUserDetailById(id) {
  return request({
    url: '/sys/user/' + id
  })
}

export function logout() {
}
