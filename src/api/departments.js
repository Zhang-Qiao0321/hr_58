import request from '@/utils/request'
// 查询企业的部门列表
export function getDepartments() {
  return request({
    url: '/company/department'
  })
}

// 根据ID删除部门
// Path： /company/department/{id}
// Method： DELETE
export function delDepartment(id) {
  return request({
    url: '/company/department/' + id,
    method: 'delete'
  })
}

// 新增部门
// Path： /company/department
// Method： POST
export function addDepartment(data) {
  return request({
    url: '/company/department',
    method: 'post',
    data
  })
}

// 根据ID查询部门详情
// Path： /company/department/{id}
// Method： GET
export function getDepartDetailById(id) {
  return request({
    url: `/company/department/${id}`,
    method: 'get'
  })
}

// 根据ID修改部门详情
// Path： /company/department/{id}
// Method： PUT
export function updateDepartments(data) {
  return request({
    url: '/company/department/' + data.id,
    method: 'put',
    data
  })
}
