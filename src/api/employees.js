import request from '@/utils/request'

// 获取员工简单列表
// Path： /sys/user/simple
// Method： GET
export function getEmployeeSimple(data) {
  return request({
    url: '/sys/user/simple',
    method: 'get'
  })
}

// 获取员工列表
// Path： /sys/user
// Method： GET
export function getEmployeeList(params) {
  return request({
    method: 'get',
    url: '/sys/user',
    params
  })
}

/**
 * 删除员工接口
 * ****/

export function delEmployee(id) {
  return request({
    url: `/sys/user/${id}`,
    method: 'delete'
  })
}

// 新增员工
// Path： /sys/user
// Method： POST
export function addEmployee(data) {
  return request({
    url: `/sys/user`,
    method: 'post',
    data
  })
}

// 批量导入员工
// Path： /sys/user/batch
// Method： POST
export function importEmployee(data) {
  return request({
    url: `/sys/user/batch`,
    method: 'post',
    data
  })
}

// 保存员工基本信息
// Path： /sys/user/{id}
// Method： PUT
export function saveUserDetailById(data) {
  return request({
    url: `/sys/user/${data.id}`,
    method: 'put',
    data
  })
}

// 获取员工个人详情信息
// Path： /employees/{id}/personalInfo
// Method： GET
export function getPersonalDetail(id) {
  return request({
    url: `/employees/${id}/personalInfo`
  })
}

// 保存用户个人详情信息
// Path： /employees/{id}/personalInfo
// Method： PUT
export function updatePersonal(data) {
  return request({
    url: `/employees/${data.userId}/personalInfo`,
    method: 'put',
    data
  })
}

// 获取员工岗位信息
// Path： /employees/{id}/transferPosition
// Method： GET
export function getJobDetail(id) {
  return request({
    url: `/employees/${id}/jobs`
  })
}

// 保存员工岗位信息
// Path： /employees/{id}/jobs
// Method： PUT
export function updateJob(data) {
  return request({
    url: `/employees/${data.userId}/jobs`,
    method: 'put',
    data
  })
}

// 给员工分配角色
// Path： /sys/user/assignRoles
// Method： PUT
export function assignRoles(data) {
  return request({
    url: '/sys/user/assignRoles',
    data,
    method: 'put'
  })
}
