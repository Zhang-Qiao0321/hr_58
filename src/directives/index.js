import Vue from 'vue'
// 注册全局指令
Vue.directive('imgError',
  {
    inserted(el, options) {
      // console.log(el)
      // console.log(options)
      // 如果图片地址不存在为空时,使用默认图片
      el.src = el.src || options.value
      el.onerror = function() {
        // 一旦当前 img 出错
        // 就将调用指令时传入的图片,替换掉当前 src 修复图片
        // el.src = Vue.prototype.$userPhoto
        el.src = options.value
      }
    },
    componentUpdated(el, options) {
      // 如果图片地址不存在为空时,使用默认图片
      el.src = el.src || options.value
    }
  })
