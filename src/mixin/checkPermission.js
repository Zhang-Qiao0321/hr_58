import store from '@/store'
export default {
  // 混入属性需要跟vue一样
  methods: {
    checkPermission(key) {
      // 这个函数接收一个想要查询功能权限 key,获取用户数据中的 roles.points 功能权限
      const { points } = store.state.user.userInfo.roles
      // console.log(points)
      // 如果 key 存在于 roles.points 中,就返回 true 否则返回 false
      return points.includes(key)
    }
  }
}
