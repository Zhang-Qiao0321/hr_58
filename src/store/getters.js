// getters中引用了user中的状态，所以我们将getters中的状态改为
const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  userId: state => state.user.userInfo.userId, // 映射用户id
  username: state => state.user.userInfo.username, // 映射用户名
  staffPhoto: state => state.user.userInfo.staffPhoto, // 映射用户头像
  companyId: state => state.user.userInfo.companyId, // 映射公司 id
  routes: state => state.permission.routes // 用户访问路由权限
}
export default getters
