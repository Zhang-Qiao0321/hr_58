// 引入公共的静态路由权限
import { asyncRoutes, constantRoutes } from '@/router/index'
const state = {
  // 这里用来管理公共路由配置
  routes: []
}
const mutations = {
  // 修改路由的state方法中的routes
  setRoutes(state, newRoutes) {
    state.routes = [...constantRoutes, ...newRoutes]
  }
}
const actions = {
  // 封装筛选路由
  filterRoutes({ commit }, roles) {
    // console.log(roles)
    // 第一种思路
    // 筛选路由逻辑: 拿到所有动态路由配置进行 filter 遍历
    const routes = asyncRoutes.filter(item => {
      // 每个动态路由中有 name 路由名称看是否在 roles.menus 路由标识中存在, 存在就返回,不存在就抛弃
      // indexOf 方法
      // return roles.menus.indexOf(item.name) === 0
      // some 方法
      // return roles.menus.some(key => key === item.name)
      // includes 方法
      // console.log(roles.menus)
      return roles.menus.includes(item.name)
    })
    // console.log(routes)

    // 将 actions 中筛选出来的用户有权限的使用的动态路由配置
    // 调用 mutations 跟之前的静态路由合并,存放 state 的 routes 当中就是当前用户可以访问的路由
    commit('setRoutes', routes)

    // 别处可能需要用到筛选后的用户权限路由,所以需要返回出去
    return routes

    // 第二种思路
    // 创建一个空数组, 用 forEach 遍历 roles.menus 路由标识
    // const routes = []
    // roles.menus.forEach(key => {
    //   // key: 路由标识
    //   // 每遍历出一个标识, filter 遍历动态路由配置找到对应的配置对象,追加到空数组,最终得到所有对应的路由配置
    //   routes.push(...asyncRoutes.filter(item => item.name === key))
    // })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
