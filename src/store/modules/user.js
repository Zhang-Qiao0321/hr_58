import { getInfo, getUserDetailById, login } from '@/api/user'
import { resetRouter } from '@/router'
import { getToken, removeToken, setTimeStamp, setToken } from '@/utils/auth'
import { Message } from 'element-ui'

const state = {
  // 2. 页面刷新初始化时,从cookies本地取出 token 值
  token: getToken(),
  // 用户信息
  userInfo: {}
}

const mutations = {
  setToken(state, data) {
    // 这里只是对 vuex 数据的处理
    // 但是没有持久化
    // 持久化的两个步骤
    // 1. 数据发生变化,将token值存放到本地 cookies 中
    setToken(data)

    state.token = data
  },
  // 设置用户信息
  setInfo(state, data) {
    state.userInfo = { ...data }
  },
  // 删除用户信息
  removeUserInfo(state) {
    // 将 state 中的用户数据(userInfo)设置为空数组
    state.userInfo = {}
  },
  // 删除token
  removeToken(state) {
    state.token = ''
    // 清除 Cookies 本地储存中的 token
    removeToken()
  }
}

const actions = {
  // 封装登录请求
  async login({ commit }, data) {
    try {
      const res = await login(data)
      // console.log(res)
      Message.success('登录成功')
      commit('setToken', res)

      // 记录登录时间
      // console.log(setTimeStamp())
      setTimeStamp()
    } catch (error) {
      console.log(error.message)
    }
  },
  // login({ commit }, data) {
  //   login(data).then(res => {
  //     console.log(res)
  //   }).catch(err => {
  //     console.dir(err)
  //     console.log(err.message)
  //   })
  // }

  // 封装获取用户信息
  async getInfo({ commit }) {
    // 获取返回的用户信息
    const res = await getInfo()
    // 获取用户头像
    const baseInfo = await getUserDetailById(res.userId)
    // console.log(baseInfo)
    // 将两个接口合并
    const baseRes = { ...res, ...baseInfo }
    // 将整个个人信息设置到用户的 vuex 数据中
    commit('setInfo', baseRes)
    // 这里返回获取到的数据
    return baseRes
  },
  // 封装退出登录
  logout({ commit }) {
    // 清除 token
    commit('removeToken')
    // 清除用户数据
    commit('removeUserInfo')
    // 登出时需要重置路由
    resetRouter()
    // vuex 的 permission 模块中的 state 的 routes 也需要重置
    // 子模块调用子模块的 actions 可以将 commit 的第三个参数设置成 {root: true} 就表示当前的 commit 不是子模块了是父模块
    commit('permission/setRoutes', [], { root: true })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
