import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import UserRoutes from '@/router/modules/user'

export const constantRoutes = [
  UserRoutes,
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/test',
    component: () => import('@/views/test'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  },
  {
    path: '/import',
    component: Layout,
    hidden: true, // 隐藏在左侧菜单中
    children: [{
      path: '', // 二级路由path什么都不写 表示二级默认路由
      component: () => import('@/views/import')
    }]
  }

  // 404 page must be placed at the end !!!
  // 页面刷新的时候，本来应该拥有权限的页面出现了404，这是因为 * 号跳转404的匹配权限放在了静态路由
  // 而动态路由在没有addRoutes之前，找不到对应的地址，就会显示404，所以我们需要将404放置到动态路由的最后
  // { path: '*', redirect: '/404', hidden: true }
]

// 引入多个模块的规则
import approvalsRouter from './modules/approvals'
import departmentsRouter from './modules/departments'
import employeesRouter from './modules/employees'
import permissionRouter from './modules/permission'
import attendancesRouter from './modules/attendances'
import salarysRouter from './modules/salarys'
import settingRouter from './modules/setting'
import socialRouter from './modules/social'

// 动态路由
export const asyncRoutes = [
  approvalsRouter,
  departmentsRouter,
  employeesRouter,
  permissionRouter,
  attendancesRouter,
  salarysRouter,
  settingRouter,
  socialRouter
]

const createRouter = () => new Router({
  // 配置打包之前的路由配置
  // hash模式 ： #后面是路由路径，特点是前端访问，#后面的变化不会经过服务器,默认为 hash 模式 例: http://127.0.0.1:8888/#/dsashboard
  // history模式：正常的/访问模式，特点是后端访问，任意地址的变化都会访问服务器 例: http://127.0.0.1:8888/dsashboard
  // 打包尝试用 history 模式,将路由的 mode 类型改成 history 即可
  mode: 'history', // require service support
  // hr是特定的前缀地址，此时我们可以配置一个 base 属性，配置为hr 例: http://127.0.0.1:8888/hr/dsashboard
  base: '/hr/', // 配置项目的基础地址
  scrollBehavior: () => ({ y: 0 }), // 管理滚动行为,如果出现滚动切换 就让页面回到顶部
  // routes: [...constantRoutes, ...asyncRoutes] // 临时合并所有路由
  // 之前是静动态路由和并,现在只是静态路由,动态路由通过筛选后添加
  routes: [...constantRoutes]
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
