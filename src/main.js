import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

// 引如封装的国际化语言配置
import i18n from '@/lang'

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control
import '@/utils/userPhoto' // 默认图片
import '@/directives' // 自定义指令

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// 注释掉mock数据的部分，删除src下的mock文件夹，我们开发的时候用不到模拟数据
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }

// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)
// 设置 ElementUI 为当前的语言,配合 i18n 插件使用
Vue.use(ElementUI, {
  // 定义 i18n 的处理方式, 是一个回调函数,默认接收需要翻译的 key 和 value 两个参数, 返回处理结果
  i18n: (key, value) => i18n.t(key, value)
})

Vue.config.productionTip = false

// 引入封装的全局组件库
import Component from '@/components'
// 进行全局注册
Vue.use(Component)

// 过滤器函数的参数默认可以接收到源数据
// const formatTime = (oldValue) => {
//   const v = oldValue.slice(0, 10)
//   // 返回最终要显示的内容
//   return v
// }
// 过滤器的注册: Vue.filter() 带上两个参数 1.过滤器名称 2.过滤器函数
// Vue.filter('formatTime', formatTime)

// 引入所有过滤器方法
import * as filter from '@/filters'
// 遍历整个过滤器方法
for (const key in filter) {
  // 全局注册过滤器方法
  Vue.filter(key, filter[key])
}

// 注册打印插件
import Print from 'vue-print-nb'
Vue.use(Print)

// 全局注册混入对象
import checkPermission from '@/mixin/checkPermission'
Vue.mixin(checkPermission)

// 4. 挂载原型
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
})
