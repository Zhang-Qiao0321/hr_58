
// 这里需要一个路由守卫, 控制页面的访问权限
import router from '@/router'
// 引入 vuex 实例, 查看数据
import store from '@/store'
import NProgress from 'nprogress' // 引入一份进度条插件
import 'nprogress/nprogress.css' // 引入进度条样式

const whiteList = ['/login', '/404']

// 全局前置路由守卫
router.beforeEach(async(to, from, next) => {
  NProgress.start() // 开启进度条s
  // 根据逻辑流程进行判断
  // 有 token ?
  if (store.getters.token) {
    if (to.path === '/login') {
      // 登录页? -> 首页
      next('/')
    } else {
      // 通过判定 vuex 中的 getters 映射出来的 userInfo 中是否有userID属性,防止重复发送请求
      if (!store.getters.userId) {
        // 获取用户信息 这里调用的是 vuex 的 ations 方法可以获取用户信息进行返回,接收用户信息数据
        const { roles } = await store.dispatch('user/getInfo')
        // 解构出用户的权限进行路由筛选
        // console.log(roles)
        // 调用 vuex 封装好的筛选路由传入 用户权限路由信息
        const routes = await store.dispatch('permission/filterRoutes', roles)
        // console.log(routes)
        // 将获取筛选后的权限路由配置添加到当前 router 路由实例配置中
        // vue-router提供了一个叫做addRoutes的API方法，这个方法的含义是动态添加路由规则
        // 将404路由放置到动态路由的最后
        router.addRoutes([...routes, { path: '*', redirect: '/404', hidden: true }])
        // 这时候早就过了匹配路由的时间,再也找不到匹配组件了,解决办法是重新跳转到当前要跳转的页面
        // console.log(to)
        next(to.path)
      } else {
        // 不是 -> 通行
        next()
      }
    }
  } else {
    // 没 token ?
    // 目标是否在白名单? -> 通行
    if (whiteList.indexOf(to.path) > -1) {
      next()
    } else {
      // 不在 -> 登录页
      next('/login')
    }
  }
  NProgress.done() // 手动强制关闭一次  为了解决 手动切换地址时  进度条的不关闭的问题
})
// 后置守卫
router.afterEach(function() {
  NProgress.done() // 关闭进度条
})
/*
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
// import router from './router'
// import store from './store'
// import { Message } from 'element-ui'
// import NProgress from 'nprogress' // progress bar
// import 'nprogress/nprogress.css' // progress bar style
// import { getToken } from '@/utils/auth' // get token from cookie
// import getPageTitle from '@/utils/get-page-title'

// NProgress.configure({ showSpinner: false }) // NProgress Configuration

// const whiteList = ['/login'] // no redirect whitelist

// router.beforeEach(async(to, from, next) => {
//   // start progress bar
//   NProgress.start()

//   // set page title
//   document.title = getPageTitle(to.meta.title)

//   // determine whether the user has logged in
//   const hasToken = getToken()

//   if (hasToken) {
//     if (to.path === '/login') {
//       // if is logged in, redirect to the home page
//       next({ path: '/' })
//       NProgress.done()
//     } else {
//       const hasGetUserInfo = store.getters.name
//       if (hasGetUserInfo) {
//         next()
//       } else {
//         try {
//           // get user info
//           await store.dispatch('user/getInfo')

//           next()
//         } catch (error) {
//           // remove token and go to login page to re-login
//           await store.dispatch('user/resetToken')
//           Message.error(error || 'Has Error')
//           next(`/login?redirect=${to.path}`)
//           NProgress.done()
//         }
//       }
//     }
//   } else {
//     /* has no token*/

//     if (whiteList.indexOf(to.path) !== -1) {
//       // in the free login whitelist, go directly
//       next()
//     } else {
//       // other pages that do not have permission to access are redirected to the login page.
//       next(`/login?redirect=${to.path}`)
//       NProgress.done()
//     }
//   }
// })

// router.afterEach(() => {
//   // finish progress bar
//   NProgress.done()
// })
