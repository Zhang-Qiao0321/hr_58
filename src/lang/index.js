// 1. 引入和安装 i18n
import Vue from 'vue'
// 国际化语言配置
// 1. 引人国际化插件
import VueI18n from 'vue-i18n'
import Cookie from 'js-cookie'
Vue.use(VueI18n)

// 引入 element-ui 语言包
import elementEN from 'element-ui/lib/locale/lang/en'
import elementZH from 'element-ui/lib/locale/lang/zh-CN'

// 引入自定义语言包
import ZH from './locale/zh'
import EN from './locale/en'

// 2. 创建实例
export default new VueI18n({
  // 2.1 默认语言 从本地储存中获取 切换后的语言
  locale: Cookie.get('language') || 'zh',
  // 字典对象
  // 2.2 字典定义
  messages: {
    zh: {
      hello: '你好',
      // 将引入的语言包结构到配置当中
      ...elementZH,
      // 结构自定义语言包
      ...ZH
    },
    en: {
      hello: 'How are you',
      ...elementEN,
      ...EN
    }
  }
})
